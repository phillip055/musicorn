require 'test_helper'

class PlaylistsSongsControllerTest < ActionController::TestCase
  setup do
    @playlists_song = playlists_songs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:playlists_songs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create playlists_song" do
    assert_difference('PlaylistsSong.count') do
      post :create, playlists_song: { playlist_id: @playlists_song.playlist_id, song_id: @playlists_song.song_id }
    end

    assert_redirected_to playlists_song_path(assigns(:playlists_song))
  end

  test "should show playlists_song" do
    get :show, id: @playlists_song
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @playlists_song
    assert_response :success
  end

  test "should update playlists_song" do
    patch :update, id: @playlists_song, playlists_song: { playlist_id: @playlists_song.playlist_id, song_id: @playlists_song.song_id }
    assert_redirected_to playlists_song_path(assigns(:playlists_song))
  end

  test "should destroy playlists_song" do
    assert_difference('PlaylistsSong.count', -1) do
      delete :destroy, id: @playlists_song
    end

    assert_redirected_to playlists_songs_path
  end
end
