class Playlist < ActiveRecord::Base
	belongs_to :user
	validates :user_id, presence: true
	has_many :playlists_song
	has_many :songs, through: :playlists_song
end
