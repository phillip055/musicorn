json.array!(@playlists_songs) do |playlists_song|
  json.extract! playlists_song, :id, :playlist_id, :song_id
  json.url playlists_song_url(playlists_song, format: :json)
end
