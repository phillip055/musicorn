class CreatePlaylistsSongs < ActiveRecord::Migration
  def change
    create_table :playlists_songs do |t|
      t.references :playlist, index: true
      t.references :song, index: true

      t.timestamps null: false
    end
    add_foreign_key :playlists_songs, :playlists
    add_foreign_key :playlists_songs, :songs
  end
end
