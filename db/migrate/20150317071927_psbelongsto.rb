class Psbelongsto < ActiveRecord::Migration
 
  	def change
    change_table :playlist_songs do |t|
    	t.belongs_to :song, :user
    end
  end
  
end
